import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { END_POINT } from './shared/shared_url';

@Injectable({
  providedIn: 'root'
})
export class Covid19Service {

  constructor(private http:HttpClient) { }
  //metodo que obtiene los datos generales mundiales
  getBrief(){
    const url = END_POINT+'jhu-edu/brief';
    return this.http.get(url);
  }

  //metodo que obtiene los datos generales por pais, obtiene como parametro el codigo iso2
  getBriefbyCountry(iso2:string){
    const url = END_POINT+`jhu-edu/latest?iso2=${iso2}&onlyCountries=true`;
    return this.http.get(url);
  }
  
  //metodo que obtiene el time lapse por pais
  getTimeSeriesByCountry(iso2:string){
    const url = END_POINT+`jhu-edu/timeseries?iso2=${iso2}&onlyCountries=true`;
    return this.http.get(url);
  }
  

}
