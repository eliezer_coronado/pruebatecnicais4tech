import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  validForm=true;
  constructor( private fb:FormBuilder) { }

  public formSubscription = this.fb.group({
    name:['',[Validators.required,Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$')]],
    date:['',[Validators.required]],
    email:['',[Validators.required]]
  })

  ngOnInit(): void {
  }

  subscription(f:any){
    console.log(this.formSubscription.value.date);
    if(this.formSubscription.valid){
      this.validForm=true;
      let nombre = this.formSubscription.value.name;
      let correo = this.formSubscription.value.email;
      let fecha = this.formSubscription.value.date;
     
      if(fecha<'1900/01/01'){
        
        Swal.fire({
          title: 'Error' ,
          text: 'La fecha no puede se menor a 1900',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }else{
        Swal.fire({
          title: 'Success' ,
          html: '<h5>Muchas gracias <strong>'+nombre+'</strong> por suscribirte a nuestro servicio</h5>'+
                ' <p>Recibirás información al correo <strong>'+correo+'</strong></p>',
          icon: 'success',
          confirmButtonText: 'Cool'
        })
      }




    }else{
      this.validForm=false;

    }


    console.log(this.formSubscription.value.name);
  }

}
