import { Component, OnInit } from '@angular/core';
import { Covid19Service } from 'src/app/service/covid19.service'
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private service:Covid19Service,
              private datePipe: DatePipe) { }
  
  //variabe par almacenar los datos mundiales
  datos:any=[];
  //objeto de los paises de centro america
  paises=[
    {'pais':'Guatemala',
      'iso2':'GT',
      'imagen':'../assets/Banderas/Guatemala.png'
    },
    {'pais':'Belice',
      'iso2':'BZ',
      'imagen':'../assets/Banderas/Belice.png'
    },
    {'pais':'El Salvador',
     'iso2':'SV',
     'imagen':'../assets/Banderas/El Salvador.png'
    },
    {'pais':'Honduras',
     'iso2':'HN',
     'imagen':'../assets/Banderas/Honduras.png'
    },
    {'pais':'Nicaragua',
     'iso2':'NI',
     'imagen':'../assets/Banderas/Nicaragua.png'
    },
    {'pais':'Costa Rica',
     'iso2':'CR',
     'imagen':'../assets/Banderas/Costa Rica.png'
    },
    {'pais':'Panama',
     'iso2':'PA',
     'imagen':'../assets/Banderas/Panama.png'
    }

  ]
  //objeto para el timelapse de cada pais
  timeSeries:any=[];
  //obteto para obtener los datos generales de un pais en concreto
  country:any={
    'confirmed':'0',
    'deaths':'0',
    'recovered':'0',
    'lastupdate':'',
    'countryregion':''
      
  };
  //objeto para guardar la fecha y los datos sobre covid
  times:any={
    'fecha':'',
    'confirmado':'',
    'fallecidos':'',
    'recuperados':''
  }
  //auxiliar para imprimir de 10 en 10 los datos de timeSeries
  ti:any=[];

  //Variables que ayudan a la paginacion
  desde:number=0;
  hasta:number=10;

  ngOnInit(): void {
    this.getBrief();
  }

  //llama al servicio de datos mundiales
  getBrief(){
    this.service.getBrief().subscribe((data:any)=>{
      this.datos=data;
    })
  }

  //obtiene los datos generales por pais por medio del codigo iso2
  getDataByCountry(iso2:string){
    this.service.getBriefbyCountry(iso2).subscribe((data:any)=>{
      this.country=data[0]; 
     }); 
     this.service.getTimeSeriesByCountry(iso2).subscribe((data:any)=>{
       this.timeSeries=[];

      Object.keys(data[0].timeseries).forEach(key => {
        let obj={
          'fecha':key,
          'datos':data[0].timeseries[key]
        }
        this.timeSeries.push(obj);
      });
      this.paginacion(0);
   
    })
  }

  //funcion de paginacion, anteriores y siguientes, recibe cantidad de +10 o -10
  paginacion(cantidad:number){

    if(this.desde + cantidad < 0) {return false;}
    if(this.hasta+cantidad>this.timeSeries.length+10){return false;}
    let aux:number = 0;
    this.desde = this.desde+cantidad;
    this.hasta = this.hasta+cantidad;

    for (let i = this.desde; i < this.hasta; i++) { 
          this.ti[aux]=this.timeSeries[i];
          aux++;
        
    }
    return true;   

  }


  //funcion para revertir las fechas de modo asc o desc
  reverseDate(){
    this.timeSeries=this.timeSeries.reverse();
    this.paginacion(0);
  }

//funcion que busca los datos de un dia en especifico de un pais previamente seleccionado
  findByDate(f:any,country:string){
    let myDate = this.datePipe.transform(f,'M/d/yy');
    let auxDate = this.timeSeries.filter(function(data:any) {
      return data.fecha == myDate;
  });
    Swal.fire({
      title: 'Pais: '+country ,
      text: 'confirmados: '+ auxDate[0].datos.confirmed+
            ' fallecidos: '+ auxDate[0].datos.deaths +
            ' recuperados: '+ auxDate[0].datos.recovered,
      icon: 'success',
      confirmButtonText: 'Cool'
    })
  }


}
